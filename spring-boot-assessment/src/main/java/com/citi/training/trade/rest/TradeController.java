package com.citi.training.trade.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trade.dao.TradeDao;
import com.citi.training.trade.model.Trade;

/**
 * REST interface for managing Trade records.
 * Class adheres to RESTful paths and response codes.
 * @author Hanseul
 * @see Trade
 * */

@RestController
@RequestMapping("/trade")
public class TradeController {
	private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
	
	@Autowired
	private TradeDao tradeDao;
	
	/**
	 * GET request to find all trades
	 * @return all trades
	 */
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<Trade> finaAll(){
		LOG.info("HTTP GET findAll()");
		return tradeDao.findAll();
	}
	
	/**
	 * GET request to find trades by id
	 * @return trades found by id
	 */
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Trade findById(@PathVariable long id) {
		LOG.info("HTTP GET findById() id is" + id);
		return tradeDao.findById(id).get();
	}	
	/**
	 * Save trade
	 * @return new trade saved/created
	 */
	@RequestMapping(method=RequestMethod.POST)
	public HttpEntity<Trade> save(@RequestBody Trade trade){
		LOG.info("HTTP POST save() trade is:"+ trade);
		return new ResponseEntity<Trade> (tradeDao.save(trade), HttpStatus.CREATED);
		
	}
	
	/**
	 * HTTP request to delete trade by id
	 */
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	@ResponseStatus(value=HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		LOG.info("HTTP DELETE delete() id is:" + id);
		tradeDao.deleteById(id);
	}
	

}
