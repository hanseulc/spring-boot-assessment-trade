package com.citi.training.trade.dao;

import com.citi.training.trade.model.Trade;

import org.springframework.data.repository.CrudRepository;

public interface TradeDao extends CrudRepository<Trade, Long>{

}
