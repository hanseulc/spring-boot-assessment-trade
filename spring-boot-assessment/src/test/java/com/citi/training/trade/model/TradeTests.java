package com.citi.training.trade.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * 
 * @author Hanseul
 * 
 * This class tests all the constructors and setters, including toString method.
 * 
 * @see Trade
 *
 */

public class TradeTests {

	private long testId = 1234;
	private String testStock = "AAPL";
	private int testBuy = 1;
	private double testPrice = 1.22;
	private int testVolume = 1000;
	
	/**
	 * Tests the default constructor and setters
	 */
	
	@Test
	public void test_Trade_defaultConstructorAndSetters() {
		Trade testTrade = new Trade();
		
		testTrade.setBuy(testBuy);
		testTrade.setId(testId);
		testTrade.setPrice(testPrice);
		testTrade.setStock(testStock);
		testTrade.setVolume(testVolume);

		assertEquals("Check if trade id == testId", testId, testTrade.getId());
		assertEquals("Check if trade buy == testBuy", testBuy, testTrade.getBuy());
		assertEquals("Check if trade stock == testStock", testStock, testTrade.getStock());
		assertEquals("Check if trade price == testPrice", testPrice, testTrade.getPrice(), 0.0001);
		assertEquals("Check if trade volume == testVolume", testVolume, testTrade.getVolume());
	}
	
	/**
	 * Tests the full constructor 
	 */
	@Test
	public void test_Trade_secondConstructor() {
		Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);
		
		assertEquals("Check if trade id == testId", testId, testTrade.getId());
		assertEquals("Check if trade buy == testBuy", testBuy, testTrade.getBuy());
		assertEquals("Check if trade stock == testStock", testStock, testTrade.getStock());
		assertEquals("Check if trade price == testPrice", testPrice, testTrade.getPrice(), 0.0001);
		assertEquals("Check if trade volume == testVolume", testVolume, testTrade.getVolume());
		
		
	}
	
	/**
	 * Tests the toString method
	 */
	
    @Test
    public void test_Trade_toString() {
        Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);

        assertTrue("toString should contain testId",
                   testTrade.toString().contains(Long.toString(testId)));
        assertTrue("toString should contain testPrice",
                testTrade.toString().contains(Integer.toString(testBuy)));
        assertTrue("toString should contain testStock",
                testTrade.toString().contains(testStock));
        assertTrue("toString should contain testCompanyName",
                testTrade.toString().contains(Double.toString(testPrice)));
        assertTrue("toString should contain testVolume",
                testTrade.toString().contains(Integer.toString(testVolume)));
    }
    
}
