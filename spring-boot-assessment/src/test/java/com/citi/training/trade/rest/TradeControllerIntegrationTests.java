package com.citi.training.trade.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trade.model.Trade;

/**
 * 
 * @author Hanseul
 * 
 * This class tests the application including sending HTTP POST to /trade
 * and do a HTTP GET to /trade
 * 
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradeControllerIntegrationTests {

	private static final Logger LOG = LoggerFactory.getLogger(
           TradeControllerIntegrationTests.class);
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void getTest_returnsTest() {
		String testStock = "AAPL";
		Double testPrice = 1.22;
		int testVolume = 1000;
		int testBuy = 1;
		
		ResponseEntity<Trade> createTradeResponse = restTemplate.postForEntity("/trade", new Trade(-1, testStock, testBuy, testPrice, testVolume), Trade.class);
		
		LOG.info("Create Trade response:" + createTradeResponse.getBody());
		assertEquals(HttpStatus.CREATED, createTradeResponse.getStatusCode());
		
		ResponseEntity<Trade> findTradeResponse = restTemplate.getForEntity("/trade" + createTradeResponse.getBody().getId(), Trade.class);
	
		LOG.info("FindById Response: "+ findTradeResponse.getBody());
	    assertEquals(HttpStatus.OK, findTradeResponse.getStatusCode());
	    assertEquals("Found Trade stock name should equal testStock", testStock, findTradeResponse.getBody().getStock());
		
	}
}
